<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE chapter PUBLIC "-//OASIS//DTD DocBook XML V4.5//EN"
"http://www.oasis-open.org/docbook/xml/4.5/docbookx.dtd">
<chapter>
  <title>Software Management</title>

  <para></para>

  <section>
    <title>Compiling and installing from source</title>

    <para>You are going to compile ncftp (an ftp client) from source and to
    install it on your system. Compiling software directly from source allows
    you to enable special features, apply patches or simply get software that
    hasn't been packaged for your distro yet. The first step is to grab the
    source code.</para>

    <qandaset>
      <qandadiv>
        <qandaentry>
          <question>
            <para>Use <command>wget</command> to download the archive at
            <uri>ftp://ftp.ncftp.com/ncftp/ncftp-3.2.5-src.tar.bz2</uri>.
            Unpack it and go to the newly created directory.</para>
          </question>

          <answer>
            <programlisting><?db-font-size 90% ?>supinfo@localhost:~$ wget ftp://ftp.ncftp.com/ncftp/ncftp-3.2.5-src.tar.bz2
supinfo@localhost:~$ tar -xf ncftp-3.2.5-src.tar.bz2 
supinfo@localhost:~$ cd ncftp-3.2.5/</programlisting>
          </answer>
        </qandaentry>

        <qandaentry>
          <question>
            <para>Configure the sources to install the binaries under
            <filename>/usr</filename> rather than
            <filename>/usr/local</filename>. If the configure script complains
            about the compiler not beeing able to produce executables, ask
            your trainer for the packages to install.</para>
          </question>

          <answer>
            <programlisting>supinfo@localhost:~/ncftp-3.2.5$ ./configure --prefix=/usr</programlisting>
          </answer>
        </qandaentry>

        <qandaentry>
          <question>
            <para>Once the configure script succeeds, proceed to
            compilation.</para>
          </question>

          <answer>
            <programlisting>supinfo@localhost:~/ncftp-3.2.5$ make</programlisting>
          </answer>
        </qandaentry>

        <qandaentry>
          <question>
            <para>When compilation finishes, proceed to installation. Can you
            install it as a regular user?</para>
          </question>

          <answer>
            <programlisting>supinfo@localhost:~/ncftp-3.2.5$ su
Password: 
root@localhost:/home/supinfo/ncftp-3.2.5# make install</programlisting>

            <para>As you need to copy files outside your home directory to
            install a program on the system, you basically need complete r/w
            access on the filesystem. Therefore, you can't do that as a
            regular user and you have to switch to root to perform this
            administrative task. However, as a regular user, you can prefectly
            install a program in your home directory.</para>
          </answer>
        </qandaentry>
      </qandadiv>
    </qandaset>
  </section>

  <section>
    <title>Package management</title>

    <para></para>

    <section>
      <title>Using apt and dpkg</title>

      <para></para>

      <qandaset>
        <qandadiv>
          <qandaentry>
            <question>
              <para>You want to install the links web browser. However, you
              don't remember the package name. Search among the packages which
              ones have "links" in their names. You're only interested in
              finding "links" in packages names, not in descriptions.</para>
            </question>

            <answer>
              <programlisting>supinfo@localhost:~$ apt-cache search --names-only links</programlisting>
            </answer>
          </qandaentry>

          <qandaentry>
            <question>
              <para>Install the <package>links</package> package on your
              system.</para>
            </question>

            <answer>
              <programlisting>root@localhost:/home/supinfo# apt-get install links</programlisting>
            </answer>
          </qandaentry>

          <qandaentry>
            <question>
              <para>You want to list all files that belongs to the
              <package>links</package> package.</para>
            </question>

            <answer>
              <programlisting>supinfo@localhost:~$ dpkg -L links</programlisting>
            </answer>
          </qandaentry>

          <qandaentry>
            <question>
              <para>You want to know which installed package provides the
              <filename>/bin/ls</filename> binary. Use <command>dpkg</command>
              to find this information.</para>
            </question>

            <answer>
              <programlisting>supinfo@localhost:~$ dpkg -S /bin/ls
coreutils: /bin/ls</programlisting>
            </answer>
          </qandaentry>

          <qandaentry>
            <question>
              <para>You want to install the package providing the
              <filename>/usr/bin/twm</filename> binary. You don't know the
              package name. Use the <command>apt-file</command>(1) command to
              find it out (install it when needed).</para>
            </question>

            <answer>
              <programlisting>supinfo@debian-master:~$ apt-file search /usr/bin/twm
twm: /usr/bin/twm</programlisting>
            </answer>
          </qandaentry>

          <qandaentry>
            <question>
              <para>You want to get information about the
              <package>coreutils</package> package. Use
              <command>dpkg</command> to display these details.</para>
            </question>

            <answer>
              <programlisting>supinfo@debian-master:~$ dpkg -s coreutils
Package: coreutils
Essential: yes
Status: install ok installed
Priority: required
Section: utils
[...]</programlisting>
            </answer>
          </qandaentry>

          <qandaentry>
            <question>
              <para>Remove <package>links</package> from your system,
              including any configuration file created by this package.</para>
            </question>

            <answer>
              <programlisting>root@localhost:/home/supinfo# apt-get purge links</programlisting>
            </answer>
          </qandaentry>
        </qandadiv>
      </qandaset>
    </section>
  </section>
</chapter>
